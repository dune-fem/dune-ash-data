# find package ImageMagick
find_package( ImageMagick REQUIRED COMPONENTS composite convert montage )

find_package( LATEX REQUIRED )

find_package( Inkscape REQUIRED )
include( UseInkscape )

function( generate_texture )
  include( CMakeParseArguments )
  cmake_parse_arguments( GENTEX "" "TARGET;TILE;OUTPUT;INPUT_DIR" "INPUT;DEPENDS" ${ARGN} )

  if( NOT GENTEX_DEPENDS )
    set( GENTEX_DEPENDS ${GENTEX_INPUT} )
  endif()

  add_custom_command(
    OUTPUT ${GENTEX_OUTPUT}
    DEPENDS ${GENTEX_DEPENDS}
    COMMAND ${ImageMagick_montage_EXECUTABLE} -background "#00000000" -geometry 64x64 -tile ${GENTEX_TILE} ${GENTEX_INPUT} ${CMAKE_CURRENT_BINARY_DIR}/${GENTEX_OUTPUT}
    WORKING_DIRECTORY ${GENTEX_INPUT_DIR}
    VERBATIM
  )
  if( GENTEX_TARGET )
    add_custom_target( ${GENTEX_TARGET} ALL DEPENDS ${GENTEX_OUTPUT} )
  endif()
endfunction()
