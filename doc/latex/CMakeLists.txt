dune_add_latex_document( general_description.tex FATHER_TARGET doc DEFAULT_SAFEPDF )
create_doc_install( ${CMAKE_CURRENT_BINARY_DIR}/general_description.pdf  ${CMAKE_INSTALL_DOCDIR}/../dune-ash general_description_safepdf )

dune_add_latex_document( math_description.tex FATHER_TARGET doc DEFAULT_SAFEPDF )
create_doc_install( ${CMAKE_CURRENT_BINARY_DIR}/math_description.pdf  ${CMAKE_INSTALL_DOCDIR}/../dune-ash math_description_safepdf )

dune_add_latex_document( technical_manual.tex FATHER_TARGET doc DEFAULT_SAFEPDF )
create_doc_install( ${CMAKE_CURRENT_BINARY_DIR}/technical_manual.pdf  ${CMAKE_INSTALL_DOCDIR}/../dune-ash technical_manual_safepdf )
